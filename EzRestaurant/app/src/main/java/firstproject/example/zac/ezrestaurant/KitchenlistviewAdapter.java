package firstproject.example.zac.ezrestaurant;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Zac on 12/18/16.
 */

public class KitchenlistviewAdapter  extends BaseAdapter {
    public KitchenlistviewAdapter(ArrayList<String> liststatus, ArrayList<String> listnote, ArrayList<String> listdetails, Activity activity) {
        this.liststatus = liststatus;
        this.listnote = listnote;
        this.listdetails = listdetails;
        this.activity = activity;
    }

    ArrayList<String> liststatus  = new ArrayList<>();
    ArrayList<String> listnote = new ArrayList<>();
    ArrayList<String> listdetails = new ArrayList<>();

    Activity activity;
    public static class ViewHolder
    {

        public TextView txtdetails;
        public ImageView img;
    }

    @Override
    public int getCount() {
       return listdetails.size();
    }

    @Override
    public Object getItem(int position) {
        return listdetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        KitchenlistviewAdapter.ViewHolder view;
        LayoutInflater inflator = activity.getLayoutInflater();

        if(convertView==null)
        {
            view = new KitchenlistviewAdapter.ViewHolder();
            convertView = inflator.inflate(R.layout.mytextview_kitchenorder, null);

            view.txtdetails = (TextView) convertView.findViewById(R.id.txtdetails);
            view.img = (ImageView)convertView.findViewById(R.id.imagedetails);


            convertView.setTag(view);
        }
        else
        {
            view = (KitchenlistviewAdapter.ViewHolder) convertView.getTag();
        }

        if(liststatus.get(position).equals("") && listnote.get(position).equals("")) {
            view.txtdetails.setText(listdetails.get(position));
            view.img.setVisibility(View.INVISIBLE);
        }
        else if(liststatus.get(position).equals("cancelled"))
        {
            view.txtdetails.setText(listdetails.get(position));
            view.txtdetails.setPaintFlags(view.txtdetails.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            view.txtdetails.setTextColor(Color.RED);
            view.img.setImageResource(R.drawable.cancel24);
        }
        else if (liststatus.get(position).equals("hurry"))
        {
            view.txtdetails.setText(listdetails.get(position));
            view.txtdetails.setTextColor(Color.GREEN);
            view.img.setImageResource(R.drawable.time24);
        }
        else if(!listnote.equals(""))
        {
            view.txtdetails.setText(listdetails.get(position));
            view.txtdetails.setTextColor(Color.YELLOW);
            view.img.setImageResource(R.drawable.note24);
        }





        return convertView;
    }
}
