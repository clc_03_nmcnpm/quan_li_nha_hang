package firstproject.example.zac.ezrestaurant;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Zac on 11/19/16.
 */

public class TableAdapter extends BaseAdapter {
    private ArrayList<String> listTable;
    private ArrayList<Integer> listFlag;
    private Activity activity;

    public TableAdapter(ArrayList<String> listCountry, ArrayList<Integer> listFlag, Activity activity) {
        this.listTable = listCountry;
        this.listFlag = listFlag;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return listTable.size();
    }

    @Override
    public Object getItem(int position) {
        return listTable.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder
    {
        public ImageView imgTableStatus;
        public TextView txtTableID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder view;
        LayoutInflater inflator = activity.getLayoutInflater();

        if(convertView==null)
        {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.single_table, null);

            view.txtTableID = (TextView) convertView.findViewById(R.id.textTable);
            view.imgTableStatus = (ImageView) convertView.findViewById(R.id.imageTable);

            convertView.setTag(view);
        }
        else
        {
            view = (ViewHolder) convertView.getTag();
        }

        view.txtTableID.setText(listTable.get(position));
        //random image for table


        view.imgTableStatus.setImageResource(listFlag.get(position));

        return convertView;
    }
}
