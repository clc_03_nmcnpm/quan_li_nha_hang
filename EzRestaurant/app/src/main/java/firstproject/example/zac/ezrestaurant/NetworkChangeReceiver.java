package firstproject.example.zac.ezrestaurant;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Zac on 11/19/16.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = "CheckNetworkStatus";
    private Activity act;

    NetworkChangeReceiver(Activity av)
    {
        this.act = av;
    }
    @Override
    public void onReceive(final Context context, final Intent intent) {

        Log.v(LOG_TAG, "Receieved notification about network status");
        if (isNetworkAvailable(context)) {
            Toast.makeText(context, "Connected to the internet", Toast.LENGTH_SHORT).show();

        }
        else {
            Toast.makeText(context, "Disconnected to the internet", Toast.LENGTH_SHORT).show();



        }

    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}




