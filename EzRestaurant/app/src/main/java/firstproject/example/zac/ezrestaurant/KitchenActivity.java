package firstproject.example.zac.ezrestaurant;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.GridView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class KitchenActivity extends AppCompatActivity {

    private GridView grid;
    private ArrayList<OrderFormat> listData = new ArrayList<>();
    private DatabaseReference rootRef;
    private ProgressDialog mProgress;

    private RecyclerView mRecyclerView;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitchen);

        //generate view
       /* grid = (GridView)findViewById(R.id.kitchen_grid);
        grid.setNumColumns(3);*/


        //load data from firebase
        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Loading tables");
        mProgress.show();
        rootRef = FirebaseDatabase.getInstance().getReference();

        retrieveData();

        //generate gridview
       /* generateGridview();*/
        mRecyclerView = (RecyclerView)findViewById(R.id.kitchen_recycleview);
        mLayoutManager = new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new KitchenviewAdapter(getApplicationContext(),listData,this);
        mRecyclerView.setAdapter(mAdapter);

    }



    private void retrieveData()
    {
        DatabaseReference orderRef = rootRef.child("ORDER");
        orderRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

              listData.clear();
                mAdapter.notifyDataSetChanged();
                for(DataSnapshot single_order: dataSnapshot.getChildren())
                {
                    OrderFormat order = single_order.getValue(OrderFormat.class);
                    if(order.getOrderedFood().size() > 0) //check if order has food cointained
                    {
                        listData.add(order);
                    }
                    mProgress.dismiss();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

}

