package firstproject.example.zac.ezrestaurant;

import android.app.Activity;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Zac on 12/17/16.
 */

public class ReviewListAdapter  extends BaseAdapter {
    private Activity activity;

    private ButtonReviewClickNotify buttonNotify;
    private ArrayList<Food> listOrder = new ArrayList<>();
    private String type;

    public ReviewListAdapter(Activity activity, ArrayList<Food> listOrder, String type ) {
        this.activity = activity;
        this.listOrder = listOrder;

       this.type = type;

        try{
            buttonNotify=(ReviewFoodActivity)activity;
        }catch(Throwable e){
            //interface is not implemented
        }

    }


    @Override
    public int getCount() {
        return listOrder.size();
    }

    @Override
    public Object getItem(int position) {
        return listOrder.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder
    {
        public TextView txtReviewdetails;
        public Button btnminus;
        public Button btnadd;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ReviewListAdapter.ViewHolder view;
        LayoutInflater inflator = activity.getLayoutInflater();

        if(convertView==null)
        {
            view = new ReviewListAdapter.ViewHolder();
            convertView = inflator.inflate(R.layout.single_reviewrow, null);

            view.txtReviewdetails = (TextView) convertView.findViewById(R.id.txt_reviewdetails);
            view.btnminus = (Button) convertView.findViewById(R.id.btn_minus);
            view.btnadd = (Button) convertView.findViewById(R.id.btn_add);

            //nếu gọi để thanh toán thì k hiện button
            if(type.equals("payment"))
            {
                view.btnminus. setVisibility(View.INVISIBLE);
                view.btnadd. setVisibility(View.INVISIBLE);
            }

            convertView.setTag(view);
        }
        else
        {

            view = (ReviewListAdapter.ViewHolder) convertView.getTag();
        }




        if(type.equals("separate"))
            generateSeparateView(position,view);
        else
            generateTextview(position,view);

        view.btnminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Food tmp = listOrder.get(position);
                int quantity = tmp.getQuantity();

                    try{
                        buttonNotify.onBUttonClick(type,position,-1,(View) v.getParent());
                        if(!type.equals("separate"))
                            generateTextview(position,view);
                    }catch (Throwable e){
                        //interface can be null
                    }



            }
        });

        view.btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    buttonNotify.onBUttonClick(type,position,1, (View) v.getParent());
                    if(!type.equals("separate"))
                        generateTextview(position,view);
                }catch (Throwable e){
                    //interface can be null
                }

            }
        });

        return convertView;
    }

    private void generateSeparateView(int pos,ReviewListAdapter.ViewHolder view) {

        view.txtReviewdetails.setText(listOrder.get(pos).getFoodname() + " - " +listOrder.get(pos).getPrize() + "đ " );
        view.btnminus.setText(Integer.toString(listOrder.get(pos).getQuantity()));

    }

    private void generateTextview(int pos,ReviewListAdapter.ViewHolder view ) {


        if(listOrder.get(pos).getDiscount() == 0f) {
            Float singleprice = Float.valueOf(Float.toString(listOrder.get(pos).getQuantity() * listOrder.get(pos).getPrize()));

            view.txtReviewdetails.setText(listOrder.get(pos).getFoodname() + " x " + listOrder.get(pos).getQuantity() + " = " + singleprice);
            if(listOrder.get(pos).getStatus().equals("cancelled"))
            {
                view.txtReviewdetails.setPaintFlags(view.txtReviewdetails.getPaintFlags() |  Paint.STRIKE_THRU_TEXT_FLAG);
            }
        }
        else
        {
            Float  totaldiscount = Float.valueOf(listOrder.get(pos).getDiscount()*listOrder.get(pos).getQuantity());

            Float singlediscount = listOrder.get(pos).getDiscount();
            Float singleprice = Float.valueOf(Float.toString(listOrder.get(pos).getQuantity() * listOrder.get(pos).getPrize()));
            view.txtReviewdetails.setText(listOrder.get(pos).getFoodname() +"- (" + Float.toString(singlediscount) + "đ)  x " + listOrder.get(pos).getQuantity() + " = "+ (singleprice - totaldiscount));
            if(listOrder.get(pos).getStatus().equals("cancelled"))
            {
                view.txtReviewdetails.setPaintFlags(view.txtReviewdetails.getPaintFlags() |  Paint.STRIKE_THRU_TEXT_FLAG);
            }
        }





    }
}
