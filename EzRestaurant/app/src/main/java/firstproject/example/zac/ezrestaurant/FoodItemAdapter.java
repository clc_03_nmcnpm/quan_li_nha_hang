package firstproject.example.zac.ezrestaurant;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Zac on 11/20/16.
 */

public class FoodItemAdapter extends BaseAdapter {

    private Activity activity;

    private ArrayList<Food> listFood ;

    public FoodItemAdapter(Activity activity, ArrayList<Food> listFood) {
        this.activity = activity;
        this.listFood = listFood;

    }

    @Override
    public int getCount() {
        return listFood.size();
    }

    @Override
    public Object getItem(int position) {
        return listFood.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder
    {
        public TextView txtQuantity;
        public TextView txtFoodName;
        public TextView txtPrice;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FoodItemAdapter.ViewHolder view;
        LayoutInflater inflator = activity.getLayoutInflater();

        if(convertView==null)
        {
            view = new FoodItemAdapter.ViewHolder();
            convertView = inflator.inflate(R.layout.single_fooditem, null);

            view.txtFoodName = (TextView) convertView.findViewById(R.id.txt_foodname);
            view.txtQuantity = (TextView) convertView.findViewById(R.id.txt_quantity);
            view.txtPrice = (TextView) convertView.findViewById(R.id.txt_price);
            convertView.setTag(view);
        }
        else
        {
            view = (FoodItemAdapter.ViewHolder) convertView.getTag();
        }

        view.txtFoodName.setText(listFood.get(position).getFoodname());
        //random image for table



            view.txtQuantity.setText("x"+listFood.get(position).getQuantity().toString());
            view.txtPrice.setText(Float.toString(listFood.get(position).getPrize()));
        return convertView;
    }
}
