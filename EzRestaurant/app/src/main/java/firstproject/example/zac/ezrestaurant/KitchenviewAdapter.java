package firstproject.example.zac.ezrestaurant;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Zac on 12/18/16.
 */

public class KitchenviewAdapter extends RecyclerView.Adapter<KitchenviewAdapter.ViewHolder> {

    private Context mContext;
    ArrayList<OrderFormat> listData = new ArrayList<>();
    Activity activity;

    public KitchenviewAdapter(Context mContext, ArrayList<OrderFormat> listData, Activity activity) {
        this.mContext = mContext;
        this.listData = listData;
        this.activity = activity;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ListView lv;
        public TextView txtOrdername,txtMoney,txtStaff,txtTime;
        public LinearLayout linearLayout;
        public ViewHolder(View v){
            super(v);
            lv = (ListView)v.findViewById(R.id.order_lv);
            txtMoney = (TextView)v.findViewById(R.id.txt_totalmoney);
            txtOrdername = (TextView)v.findViewById(R.id.txt_ordername);
            txtTime = (TextView)v.findViewById(R.id.txt_time);
            txtStaff = (TextView)v.findViewById(R.id.txt_staff);
        }
    }

    @Override
    public KitchenviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.single_orderitem,parent,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(KitchenviewAdapter.ViewHolder holder, int position) {

        holder.txtStaff.setText(listData.get(position).getStaffname());
        String[] separated = listData.get(position).getOrderedtime().split(">");
        holder.txtTime.setText(separated[1]); //set time only ( split date)
        holder.txtOrdername.setText(listData.get(position).getSpname());
        ArrayList<String> listString = new ArrayList<>();
        ArrayList<String> listnote = new ArrayList<>();
        ArrayList<String> liststatus = new ArrayList<>();

        Float totalprice = 0f;
        for(Food a:listData.get(position).getOrderedFood()) {
            if(a.getQuantity() > 0) {
                listString.add(a.getFoodname() + "x" + a.getQuantity());
                totalprice += a.getPrize() * a.getQuantity();
                listnote.add(a.getNote());
                liststatus.add(a.getStatus());

            }
        }

        /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,R.layout.mytextview_kitchenorder, listString);*/
        KitchenlistviewAdapter adapter = new KitchenlistviewAdapter(liststatus,listnote,listString,activity);
        holder.txtMoney.setText(Float.toString(totalprice));
        holder.lv.setAdapter(adapter);
        setListViewHeightBasedOnChildren(holder.lv);
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        int i;
        for (i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();

        }

        //add divider height to total height as many items as there are in listview
        totalHeight += listView.getDividerHeight()*i;
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() )) + 150;
        listView.setLayoutParams(params);

    }
}
