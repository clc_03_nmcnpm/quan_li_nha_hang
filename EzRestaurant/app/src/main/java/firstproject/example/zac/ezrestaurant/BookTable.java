package firstproject.example.zac.ezrestaurant;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BookTable extends AppCompatActivity {

    private TableAdapter mTableAdapter;
    private ArrayList<String> listTable = new ArrayList<>();
    private ArrayList<Integer> listImg = new ArrayList<>();
    private DatabaseReference mRef;
    private DatabaseReference rootRef;
    private ProgressDialog mProgress;
    private BroadcastReceiver broadcastReceiver;
    private int TABLE_INDEX = 0;
    private GridView gridView;
    private NetworkChangeReceiver networkreceiver;
    private String Staffname;
    private String SP_NAME;
    private ArrayList<OrderBriefDescription> listBOrder = new ArrayList<>();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.kitchen:
            {
                Intent i = new Intent(BookTable.this,KitchenActivity.class);
                startActivity(i);
                break;
            }
            case R.id.logout:
            {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.saved_staff_name),"null" );
                editor.commit();

                Intent i = new Intent(BookTable.this,Login.class);
                startActivity(i);
                finish();
                break;
            }

        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_table);

        //get username from Login
        Intent intent = getIntent();
        Staffname = intent.getStringExtra("Staff");

        networkreceiver = new NetworkChangeReceiver(this);


        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkreceiver, filter);


        mProgress = new ProgressDialog(BookTable.this);
        mProgress.setTitle("Loading tables");
        mProgress.show();

        if(!isOnline())
        {
            mProgress.dismiss();
            Toast.makeText(this, "No internet access", Toast.LENGTH_SHORT).show();
        }

        rootRef = FirebaseDatabase.getInstance().getReference();
        mRef = rootRef.child("NUM_TABLE");
        //connect to Database
        if(savedInstanceState == null ) {

            this.retrieveData();
        }
        else // avoid loss data when screen-rotate
        {
            mProgress.dismiss();
            listImg = (ArrayList<Integer>) savedInstanceState.getSerializable("list_img");
            listTable = (ArrayList<String>) savedInstanceState.getSerializable("list_table");
            listBOrder =   savedInstanceState.getParcelableArrayList("list_brieforder");
        }
        //connect to resource

        gridView = (GridView)findViewById(R.id.gridView1);

        mTableAdapter = new TableAdapter(listTable,listImg,this);
        gridView.setAdapter(mTableAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(BookTable.this, Integer.toString(position), Toast.LENGTH_SHORT).show();
               /* ImageView imgchange = (ImageView)view.findViewById(R.id.imageTable);
                imgchange.setImageResource(R.drawable.common_ic_googleplayservices);
                listImg.set(position,R.drawable.common_ic_googleplayservices);
                imgchange.invalidate();*/
                int intRes = getResources().getIdentifier("ic_directions_run_black_24dp", "drawable", getPackageName());
                if(listImg.get(position) == intRes )
                {
                    OpenSPDialog(position);

                }
                else {
                    TABLE_INDEX = position;
                    SelectGuestDialog();
                }

            }
        });
    }

    private void OpenSPDialog(final int tableindex) {
        final Dialog dl = new Dialog(this);
        dl.setContentView(R.layout.dialog_choosesp);
        dl.setTitle("Chọn hóa đơn");



        ListView lv = (ListView)dl.findViewById(R.id.spname_listview);
        final ArrayList<String> listsptitle = new ArrayList<>();
        for(OrderBriefDescription tmp:listBOrder) {
            if(tmp.getTableindex() == tableindex)
                listsptitle.add(tmp.getSpname() + "> " + tmp.getTotalpayment() + "đ");
        }

        if(listsptitle.size() == 0) {
            Intent intent = new Intent(BookTable.this, ReviewFoodActivity.class);
            intent.putExtra("CALL_ACTIVITY", 1001);//gọi tính tiền
            intent.putExtra("TABLE_INDEX", tableindex);
            intent.putExtra("SPNAME", ""); //"" nghĩa là không có tách bill
            startActivity(intent);



        }
        else if(listsptitle.size() == 1)
        {
            Intent intent = new Intent(BookTable.this, ReviewFoodActivity.class);
            intent.putExtra("CALL_ACTIVITY", 1001);//gọi tính tiền
            intent.putExtra("TABLE_INDEX", tableindex);

            String[] separated = listsptitle.get(0).split(">");

            SP_NAME = String.valueOf(separated[0] );
            intent.putExtra("SPNAME", SP_NAME); //"" nghĩa là không có tách bill
            startActivity(intent);
        }
        else {

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listsptitle);
            lv.setAdapter(adapter);
            dl.show();

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String[] separated = listsptitle.get(position).split(">");

                    SP_NAME = String.valueOf(separated[0] );
                    dl.dismiss();
                    Intent intent = new Intent(BookTable.this, ReviewFoodActivity.class);
                    intent.putExtra("CALL_ACTIVITY", 1001);//gọi tính tiền
                    intent.putExtra("TABLE_INDEX", tableindex);
                    intent.putExtra("SPNAME", SP_NAME); //"" nghĩa là không có tách bill
                    startActivity(intent);
                }
            });
        }


    }

    //save list img&data before status change
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Make sure to call the super method so that the states of our views are saved
        super.onSaveInstanceState(outState);
        // Save our own state now
        outState.putSerializable("list_img", listImg);
        outState.putSerializable("list_table", listTable);
        outState.putParcelableArrayList("list_brieforder",listBOrder);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(networkreceiver);
        super.onDestroy();
    }

    //generat list data when receive NUM_TABLE from firebase
    public void generate(int Numtable)
    {
        listImg.clear();
        listTable.clear();

        for(int i =0;i<Numtable;i++)
        {
            String id = Integer.toString(i);
            listTable.add(id);

            listImg.add(R.drawable.ic_local_dining_black_24dp);


        }
    }

    //Select num of guests custom dialog controller
    public void SelectGuestDialog()
    {
        final Dialog dl = new Dialog(this);
        dl.setTitle("Choose Guest");
        dl.setContentView(R.layout.customdialog_selectguest);
        dl.show ();

        for(int i =1 ;i<=12;i++)
        {
            int resId = getResources().getIdentifier("btn" + i, "id", getPackageName());
            final Button btn = (Button) dl.findViewById(resId);

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(BookTable.this, btn.getText(), Toast.LENGTH_SHORT).show();
                    dl.dismiss();
                    Intent intent = new Intent(getBaseContext(), OrderActivity.class);

                    intent.putExtra("TABLE_INDEX", TABLE_INDEX);
                    intent.putExtra("NUMS_GUEST",btn.getText());
                    intent.putExtra("Staff",Staffname);
                    intent.putExtra("TYPE",900);
                    startActivity(intent);
                }
            });

        }

    }

    //retrieveData
    private void retrieveData()
    {
        rootRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
              /*  mProgress = new ProgressDialog(BookTable.this);
                mProgress.setTitle("Updating tables");
                mProgress.show();
*/
                if(Update(dataSnapshot) == true)

                    mProgress.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference detailsRef = rootRef.child("details");
        detailsRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                listBOrder.clear();
                for(DataSnapshot single_brieforder: dataSnapshot.getChildren())
                {

                    if(!single_brieforder.child("spname").getValue().equals(""))
                        listBOrder.add(single_brieforder.getValue(OrderBriefDescription.class));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private boolean Update(DataSnapshot dataSnapshot) {
        int Numtable =  dataSnapshot.child("NUM_TABLE").getValue(int.class);
        Toast.makeText(this, "update", Toast.LENGTH_SHORT).show();
        generate(Numtable);
        DataSnapshot details = dataSnapshot.child("details");
        for(DataSnapshot tmp:details.getChildren())
        {
            int table_index = tmp.child("tableindex").getValue(int.class);

            listImg.set(table_index,R.drawable.ic_directions_run_black_24dp);

        }
        mTableAdapter.notifyDataSetChanged();
        if(Numtable != 0)
            return true;
        return false;
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }


}