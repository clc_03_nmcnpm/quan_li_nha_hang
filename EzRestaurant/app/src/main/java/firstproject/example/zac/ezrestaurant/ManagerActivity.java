package firstproject.example.zac.ezrestaurant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by USER on 1/3/2017.
 */

public class ManagerActivity extends AppCompatActivity {

    private ImageButton imbAddstaff, imbAdddish, imbBook, imbLogout;
    private  String staff;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);

        Intent intent = getIntent();
        staff = intent.getStringExtra("Staff");
        imbAddstaff = (ImageButton) findViewById(R.id.imbAddstaff);
        imbAdddish = (ImageButton) findViewById(R.id.imbAdddishes);
        imbBook = (ImageButton) findViewById(R.id.imbBook);
        imbLogout = (ImageButton)findViewById(R.id.imbLogout);
    }

    @Override
    protected void onStart() {
        super.onStart();

        imbAddstaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),SignUp.class);
                startActivity(intent);
            }
        });

        imbAdddish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imbBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),BookTable.class);
                intent.putExtra("Staff",staff);
                startActivity(intent);
            }
        });

        imbLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),Login.class);
                startActivity(intent);
            }
        });


    }
}