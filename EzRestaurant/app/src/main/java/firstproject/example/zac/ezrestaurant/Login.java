package firstproject.example.zac.ezrestaurant;

/**
 * Created by USER on 11/9/2016.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;


class dataAccount{

    private  String user;
    private  String pass;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private  String type;
    public dataAccount(String user, String pass,String type) {
        this.user = user;
        this.pass = pass;
        this.type = type;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPass() {

        return pass;
    }

    public String getUser() {
        return user;
    }
};
public class Login extends AppCompatActivity {


    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private DatabaseReference rootRef;
    @InjectView(R.id.input_email)    EditText _emailText;
    @InjectView(R.id.input_password) EditText _passwordText;
    @InjectView(R.id.btn_login) Button _loginButton;
    private ArrayList <dataAccount> ListCheckExitsAcc  = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.inject(this);
        rootRef = FirebaseDatabase.getInstance().getReference();
        //nếu đã đang nhập không cần dang nhap lai
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String staffname = sharedPref.getString(getString(R.string.saved_staff_name), "null");
        if(!staffname.equals("null"))
        {

            String type =  sharedPref.getString("saved_type", "null");
            if(type.equals("staff")) {
                Intent intent = new Intent(getBaseContext(), BookTable.class);
                intent.putExtra("Staff", staffname);
                startActivity(intent);
                finish();
            }
            else if(type.equals("manager"))
            {
                Intent intent = new Intent(getBaseContext(), ManagerActivity.class);
                startActivity(intent);
                finish();
            }
        }
        RecieveData();

        _loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckAccount();
            }
        });
    }


    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(Login.this,
                R.style.AppTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        // TODO: Implement your own authentication logic here.



        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }


    public void onLoginSuccess() {

        _loginButton.setEnabled(true);
        finish();
    }

    public void onLoginFailed() {
      /*  Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();*/

        _loginButton.setEnabled(true);
    }


    private void  RecieveData(){
        rootRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DataSnapshot staffdata = dataSnapshot.child("Staff");

                for(DataSnapshot singleStaff: staffdata.getChildren())
                {
                    String tmpUser,tmpPassword,tmptype;
                    tmpUser = singleStaff.child("username").getValue(String.class);
                    tmpPassword = singleStaff.child("password").getValue(String.class);
                    tmptype = singleStaff.child("type").getValue(String.class);

                    dataAccount data = new dataAccount(tmpUser,tmpPassword,tmptype);
                    ListCheckExitsAcc.add(data);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }
    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    private void CheckAccount()
    {
        String input_user,input_password;
        input_user = _emailText.getText().toString().trim();
        input_password = _passwordText.getText().toString().trim();

        for(dataAccount tmp: ListCheckExitsAcc)
        {
            if(input_user.equals(tmp.getUser()) && input_password.equals(tmp.getPass()) )
            {
                //SAVE STAFF TO Shared Preferences
                String type = tmp.getType().toString().trim();
                SaveStaff2SharedPref(input_user,type);


                // INTENT TO DIFFERENT ROLE
                if(type.equals("staff")) {
                    Intent intent = new Intent(getBaseContext(), BookTable.class);
                    intent.putExtra("Staff", input_user);
                    startActivity(intent);
                    finish();
                }
                else if(type.equals("manager"))
                {
                    Intent intent = new Intent(getBaseContext(), ManagerActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
            else
            {
                onLoginFailed();
            }


        }
    }

    private void SaveStaff2SharedPref(String input_user,String staff_type) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.saved_staff_name),input_user );
        editor.commit();

      /*  SharedPreferences sharedPref2 = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor2= sharedPref2.edit();
        editor2.putString(getString(R.string.saved_staff_type),staff_type );
        editor2.commit();*/

        SharedPreferences.Editor editor2= sharedPref.edit();
        editor2.putString("saved_type",staff_type );
        editor2.commit();
    }

}