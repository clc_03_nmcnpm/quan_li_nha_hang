package firstproject.example.zac.ezrestaurant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class  SignUp extends AppCompatActivity {

    private Boolean valid_user = true;
    private EditText et_realname;
    private EditText et_username;
    private EditText et_password;
    private TextView tv_usercheck;
    private String realname, type = "abc";
    private String username;
    private ArrayList<String> staffType = new ArrayList<>(Arrays.asList("-","Quản lí", "Nhân viên"));
    private NumberPicker npType;
    private String password;
    private Button btn_signin;
    private DatabaseReference Staffroot;
    private DatabaseReference rootRef;
    private ArrayList<String> list_staff = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        et_realname = (EditText)findViewById(R.id.input_realname);
        et_password = (EditText)findViewById(R.id.input_password);
        et_username = (EditText)findViewById(R.id.input_username);
        tv_usercheck = (TextView)findViewById(R.id.txt_usernamecheck);
        tv_usercheck.setCursorVisible(false);
        et_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                {
                    username = et_username.getText().toString().trim();
                    if(!isValidUser(username)) {
                        Toast.makeText(SignUp.this, "Đã có user này", Toast.LENGTH_SHORT).show();
                        tv_usercheck.setText("Đã có usNhân viêner này");
                        tv_usercheck.setCursorVisible(true);
                    }
                    else
                        tv_usercheck.setCursorVisible(false);

                }
                else
                {
                    et_username.invalidate();
                }

            }
        });
        btn_signin = (Button)findViewById(R.id.btn_signup);
        npType = (NumberPicker)findViewById(R.id.input_type);
        npType.setMaxValue(staffType.size()-1);
        npType.setMinValue(0);
        npType.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return staffType.get(value);
            }
        });

        npType.setWrapSelectorWheel(false);
        npType.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if(staffType.get(newVal).equals("Quản lí"))
                    type = "manager";
                if(staffType.get(newVal).equals("Nhân viên"))
                    type = "staff";
                if(staffType.get(newVal).equals("-"))
                    type = "-";

            }
        });

        rootRef = FirebaseDatabase.getInstance().getReference();
        Staffroot = rootRef.child("Staff");
        ReceiveData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = et_username.getText().toString().trim();
                password = et_password.getText().toString().trim();
                realname = et_realname.getText().toString().trim();

                if(!username.equals(null) && !password.equals(null) && !realname.equals(null) && !staffType.get(npType.getValue()).equals("-") && !type.equals("abc"))
                {


                    StaffDetail pusher = new StaffDetail(realname, password, username,type);
                    Staffroot.push().setValue(pusher).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.v("Sign up status", "push finished");
                            Toast.makeText(SignUp.this, "Đã đăng ký thành công", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getBaseContext(), ManagerActivity.class);
                            startActivity(intent);
                        }
                    });
                }
                else
                {
                    Toast.makeText(SignUp.this, "Username trùng, xin đăng ký lại", Toast.LENGTH_SHORT).show();
                }
            }


        });
    }

    private void ReceiveData() {

        rootRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DataSnapshot datastaff = dataSnapshot.child("Staff");
                for (DataSnapshot single_staff : datastaff.getChildren()) {
                    list_staff.add(single_staff.child("username").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private boolean isValidUser(final String username)
    {
        for(String single_staff: list_staff)
        {
            if(username.equals(single_staff))
            {
                Toast.makeText(SignUp.this, "Đã có username này", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

}

class StaffDetail {

    public StaffDetail(String realname, String password, String username, String type) {
        this.realname = realname;
        this.password = password;
        this.username = username;
        this.type = type;
    }

    private String realname;
    private String username;
    private String password;
    private  String type;

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getRealname() {   return realname;    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
    public String getType() {     return type;
    }
}
