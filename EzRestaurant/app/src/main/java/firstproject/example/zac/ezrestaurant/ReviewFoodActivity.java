package firstproject.example.zac.ezrestaurant;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import static android.text.InputType.TYPE_CLASS_NUMBER;

public class ReviewFoodActivity extends AppCompatActivity implements ButtonReviewClickNotify {


    private TextView txtStaff;
    private TextView txtTotal;
    private ListView lvOrderItems;
  /*  private ArrayAdapter<String> adapter;*/
    private ReviewListAdapter adapter,separateadapter;
    private Button btnSend;
    private DatabaseReference rootRef;
    private DatabaseReference orderRef;
    private ArrayList<String> listOrdered = new ArrayList<>();
    private ArrayList<Food> listData = new ArrayList<>();
    private OrderFormat myOrder;
    private String Staff,currentspname;
    private int tableindex,numsguest, countsp = 0;
    private Float totalprice;
    private ProgressDialog mProgress;
    private LinearLayout llayout;
    private Button btn_popup1,btn_popup2;
    private GridLayout gridaddon;
    private  int screenwidth,index =-1;
    private ArrayList<Food> listSeparate = new ArrayList<Food>();
    private ArrayList<Food> listchild = new ArrayList<Food>();
    private ArrayList<Integer> cancaledList = new ArrayList<>();


    private Dialog spdialog;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_review_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_printbill:
            {
                OnPrintBill();
            }
            case R.id.menu_addbill: {

                spdialog = new Dialog(ReviewFoodActivity.this);
                spdialog.setContentView(R.layout.dialog_seperatebill);
                spdialog.setTitle("Tách hóa đơn");
                spdialog.show ();

                ListView mylist = (ListView)spdialog.findViewById(R.id.listview) ;
                try {
                    generateListSeparate(mylist);
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }

                Button ok = (Button)spdialog.findViewById(R.id.btn_ok3);
                Button no = (Button)spdialog.findViewById(R.id.btn_dismis);

                TextView left1 = (TextView) spdialog.findViewById(R.id.txt_left1);
                TextView left2 = (TextView) spdialog.findViewById(R.id.txt_left2);

                TextView right1 = (TextView) spdialog.findViewById(R.id.txt_right1);
                final TextView right2 = (TextView) spdialog.findViewById(R.id.txt_right2);
                right2.setText("Số lượng: 0");
                left2.setText("Số lượng: 0");

                right1.setText("Tổng tiền: 0");
                left1.setText("Tổng tiền: 0");
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //delete old order from firebase
                        if(!right2.getText().toString().trim().equals("Số lượng: 0")) {
                            OnSPButtonClick();
                            spdialog.dismiss();
                            finish();
                        }
                        else
                            Toast.makeText(ReviewFoodActivity.this, "Không thể tách bill", Toast.LENGTH_SHORT).show();

                    }
                });
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        spdialog.dismiss();
                    }
                });
                return true;
            }
            case R.id.menu_divbill: {
                Intent i = new Intent(ReviewFoodActivity.this,OrderActivity.class);
                i.putExtra("Staff",Staff);
                i.putExtra("NUMS_GUEST",Integer.toString(numsguest));
                i.putExtra("TABLE_INDEX",tableindex);
                i.putExtra("TYPE",1002);
                startActivity(i);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void OnPrintBill() {
        Document document = new Document();
        File pdfFile = new File(Environment.getExternalStorageDirectory().getPath(), "/Hello.pdf");
        /*String file = Environment.getExternalStorageDirectory().getPath() + "/Hello.pdf";*/
        try {
            PdfWriter.getInstance(document,new FileOutputStream(pdfFile));
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();
        String details = new String();
        for(String food: listOrdered )
        {
            details = details + food + "\n";
        }
        Paragraph p = new Paragraph(details);
        try {
            document.add(p);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        //PDF file is now ready to be sent to the bluetooth printer using PrintShare
       /* Intent i = new Intent(Intent.ACTION_VIEW);
        i.setPackage("com.dynamixsoftware.printershare");
        i.setDataAndType(Uri.fromFile(pdfFile),"application/pdf");*/
        Intent printIntent = new Intent("org.androidprinting.intent.action.SEND");

        startActivity(printIntent);

        document.close();


    }

    public String randomString(int length)
    {
        char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < length; i++)
        {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        String random_string = sb1.toString();

        return random_string;
    }
    private void OnSPButtonClick() {
        //delete old order

        DeleteOldOrder();

        //push 2 new orders to firebase
        OrderFormat order1 = new OrderFormat();
        OrderFormat order2 = new OrderFormat();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd>HH:mm:ss");
        String strDate = sdf.format(c.getTime());

        order1.setStaffname(Staff);
        order1.setOrderedFood(listSeparate);
        order1.setTableindex(tableindex);
        order1.setNumguest(numsguest);
        order1.setOrderedtime(strDate);
        String sp1name = "Table:" + tableindex + " - (sp" + randomString(4) + ")";
        order1.setSpname(sp1name);

        String sp2name = "Table:" + tableindex + " - (sp" +(randomString(4))+ ")";
        order2.setSpname(sp2name);
        order2.setStaffname(Staff);
        order2.setOrderedFood(listchild);
        order2.setTableindex(tableindex);
        order2.setNumguest(numsguest);
        order2.setOrderedtime(strDate);


        orderRef.push().setValue(order1);
        orderRef.push().setValue(order2);

        OrderBriefDescription setter1  = new OrderBriefDescription();
        OrderBriefDescription setter2  = new OrderBriefDescription();
        Float totalsp1 =0f,totalsp2 = 0f;
        for(Food f1:listSeparate)
            totalsp1 += f1.getPrize()*f1.getQuantity();
        for(Food f1:listchild)
            totalsp2 += f1.getPrize()*f1.getQuantity();

        setter1.setSpname(sp1name);
        setter1.setTotalpayment(totalsp1);
        setter1.setTableindex(tableindex);

        setter2.setSpname(sp2name);
        setter2.setTotalpayment(totalsp2);
        setter2.setTableindex(tableindex);

        rootRef.child("details").push().setValue(setter1);
        rootRef.child("details").push().setValue(setter2);




    }

    private void DeleteOldOrder() {
        DatabaseReference order_payment = rootRef.child("ORDER");
        order_payment.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot singleorder : dataSnapshot.getChildren()) {
                    if (singleorder.child("tableindex").getValue(Integer.class) == tableindex &&
                            singleorder.child("spname").getValue(String.class).equals(currentspname)  ) {
                        singleorder.getRef().removeValue();
                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //del from details
        DatabaseReference details = rootRef.child("details");
        details.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot single_detail:dataSnapshot.getChildren())
                {
                    //nếu bàn không có tách hóa đơn thì delete
                    if(single_detail.child("tableindex").getValue(int.class) == tableindex &&
                            single_detail.child("spname").getValue(String.class).equals(currentspname))
                    {
                        single_detail.getRef().removeValue();
                        countsp+=1;
                    }
                    else if(single_detail.child("tableindex").getValue(int.class) ==tableindex &&
                            !(single_detail.child("spname").getValue().equals("")))
                    {
                        countsp += 1;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void generateListSeparate(ListView mylist) throws CloneNotSupportedException {

        listSeparate.clear();
        listchild.clear();
        //clone list order
        /*listSeparate.addAll(listData);
        listchild.addAll(listData);*/
        for(Food a:listData)
        {
            listSeparate.add((Food) a.clone());
            listchild.add((Food) a.clone());
        }
        for(Food a:listchild)
            a.setQuantity(0);

        separateadapter = new ReviewListAdapter(this,listSeparate,"separate");
        mylist.setAdapter(separateadapter);


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_food);


        //TEST VAL


        mProgress = new ProgressDialog(ReviewFoodActivity.this);
        mProgress.setTitle("Loading tables");

        Intent intent = getIntent();
        currentspname = intent.getStringExtra("SPNAME");
        tableindex = -1;
        numsguest = 0;
        totalprice = Float.valueOf(0);
        rootRef = FirebaseDatabase.getInstance().getReference();
        btnSend = (Button) findViewById(R.id.btn_upload);
        txtStaff = (TextView) findViewById(R.id.txt_Staff);
        txtTotal = (TextView) findViewById(R.id.txt_totalprice);
        lvOrderItems = (ListView) findViewById(R.id.list_Ordered);
        llayout = (LinearLayout) findViewById(R.id.my_linear);





       /* adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listOrdered);*/


        if (intent != null) {
            if (intent.getIntExtra("CALL_ACTIVITY", -1) == 1001) //gọi tính tiền
            {



                currentspname = intent.getStringExtra("SPNAME"); //lấy tên order;

                mProgress.show();
                tableindex = intent.getIntExtra("TABLE_INDEX", -1);
                retrieveData();

                adapter = new ReviewListAdapter(this, listData, "payment");

                lvOrderItems.setAdapter(adapter);
                lvOrderItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (index == position) {
                            llayout.removeAllViews();
                            parent.getChildAt(position).setBackgroundColor(Color.WHITE);
                            index = -1;
                        } else {

                           for(int i =0;i< parent.getCount();i++)
                           {
                               if(parent.getChildAt(i) != null)
                                    parent.getChildAt(i).setBackgroundColor(Color.WHITE);
                           }
                            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                            GeneratePopupBtn("Loại Bỏ", "Giảm giá");


                            index = position;
                            OnPopup1Click(position);
                            OnPopup2Click(position);
                        }
                    }
                });


            }

            //gọi kiểm tra order
            else {

                listData = intent.getParcelableArrayListExtra("ORDERLIST");
                Staff = intent.getStringExtra("STAFF");
                tableindex = intent.getIntExtra("TABLE_INDEX", -1);
                numsguest = intent.getIntExtra("NUMS_GUEST", 0);
                generateListOrdered();
                txtTotal.setText(Float.toString(totalprice));

                orderRef = rootRef.child("ORDER");
                txtStaff.setText(Staff);


                adapter = new ReviewListAdapter(this, listData, "checkorder");

                lvOrderItems.setAdapter(adapter);

                //khi click vào list
                lvOrderItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (index == position) {
                            llayout.removeAllViews();
                            parent.getChildAt(position).setBackgroundColor(Color.WHITE);
                            index = -1;
                        }
                        else {
                            for (int i = 0; i < parent.getCount(); i++) {
                                parent.getChildAt(i).setBackgroundColor(Color.WHITE);
                            }
                            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));

                            GeneratePopupBtn("Số lượng", "Ghi chú");
                            index = position;
                            OnPopup1Click(position);

                            for(int pos: cancaledList)
                            {
                                ChangeItemOnFirebase(pos);

                            }
                            cancaledList.clear();
                            OnPopup2Click(position);

                        }
                    }
                });



                OnBtnSendClick();

            }

        }
    }

    private void OnPopup1Click(final int position) {

        btn_popup1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn_popup1.getText().equals("Loại Bỏ"))
                {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(ReviewFoodActivity.this);
                    builder1.setMessage("Bạn có chắc muốn xóa món này ?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    listData.get(position).setStatus("cancelled");
                                    listData.get(position).setQuantity(0);
                                    adapter.notifyDataSetChanged();
                                    generateListOrdered();
                                    txtTotal.setText(Float.toString(totalprice));
                                    dialog.cancel();
                                    llayout.removeAllViews();

                                    cancaledList.add(position);

                                }
                            });

                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else if(btn_popup1.getText().equals("Số lượng"))
                {

                    final Dialog dl = new Dialog(ReviewFoodActivity.this);
                    dl.setContentView(R.layout.dialog_note);
                    dl.setTitle("Số lượng");
                    dl.show ();

                    final EditText edit = (EditText)dl.findViewById(R.id.edit_note);
                    edit.setInputType(TYPE_CLASS_NUMBER);
                    edit.setText("");
                    edit.setHint("Số lượng...");
                    final Button btn = (Button)dl.findViewById(R.id.btn_ok1);

                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Food tmp = listData.get(position);
                            tmp.setQuantity(Integer.parseInt(edit.getText().toString()));
                            listData.set(position,tmp);

                            generateListOrdered();
                            txtTotal.setText(Float.toString(totalprice));


                            adapter.notifyDataSetChanged();
                            dl.dismiss();

                        }
                    });
                }
                else
                    return;
            }
        });
    }

    private void ChangeItemOnFirebase(final int position) {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setTitle("Loading");
        pDialog.show();
        DatabaseReference orderRef = rootRef.child("ORDER");
        orderRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot a:dataSnapshot.getChildren())
                {
                    if(a.child("spname").getValue(String.class).equals(currentspname) &&
                            a.child("tableindex").getValue(int.class).equals(tableindex))
                    {
                       a.child("orderedFood").child(Integer.toString(position)).child("status").getRef().setValue("cancelled");
                        pDialog.dismiss();
                        return;

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                pDialog.dismiss();
            }
        });

    }

    private void OnPopup2Click(final int position) {

        btn_popup2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn_popup2.getText().equals("Giảm giá"))
                {

                    final Dialog dl = new Dialog(ReviewFoodActivity.this);
                    dl.setContentView(R.layout.dialog_discount);
                    dl.setTitle("Giảm giá");
                    dl.show ();

                    Button btnok = (Button)dl.findViewById(R.id.btn_ok);
                    final EditText edit = (EditText)dl.findViewById(R.id.edit);

                    final float[] kq = {0f};
                    final TextView txtdiscount = (TextView)dl.findViewById(R.id.txt_discount);
                    final TextView txtprice= (TextView)dl.findViewById(R.id.txt_price);

                    txtprice.setText("Tổng cộng: " + Float.toString(listData.get(position).getPrize()));
                    edit.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                            if(s.length()!= 0)
                            {
                                if(Integer.parseInt(edit.getText().toString()) <= 100) {
                                    kq[0] = listData.get(position).getPrize() * Float.parseFloat(edit.getText().toString()) / 100;
                                    txtdiscount.setText(Float.toString(kq[0]));
                                }
                                else
                                {
                                    txtdiscount.setText("0đ");
                                }
                            }
                            else
                            {
                                txtdiscount.setText("");
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Food tmp = listData.get(position);
                            tmp.setDiscount(kq[0]);
                            listData.set(position,tmp);
                            adapter.notifyDataSetChanged();

                            totalprice -= kq[0]*listData.get(position).getQuantity();
                            txtTotal.setText(Float.toString(totalprice));

                            dl.dismiss();

                        }
                    });


                }
                else if(btn_popup2.getText().equals("Ghi chú"))
                {

                    final Dialog dl = new Dialog(ReviewFoodActivity.this);
                    dl.setContentView(R.layout.dialog_note);
                    dl.setTitle("Ghi chú");
                    dl.show ();

                    final EditText edit = (EditText)dl.findViewById(R.id.edit_note);
                    final Button btn = (Button)dl.findViewById(R.id.btn_ok1);
                    edit.setHint("Ghi chú...");
                    edit.setText("");

                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Food tmp = listData.get(position);
                            tmp.setNote(edit.getText().toString());
                            listData.set(position,tmp);

                            adapter.notifyDataSetChanged();
                            dl.dismiss();

                            generateListOrdered();
                            txtTotal.setText(Float.toString(totalprice));

                        }
                    });
                }
                else
                    return;
            }
        });
    }
    //get firebase reference

    void GeneratePopupBtn(String txtbtn1,String txtbtn2)
    {
        llayout.removeAllViews();
        screenwidth = getWindowManager().getDefaultDisplay().getWidth();
        btn_popup1 = new Button(this);
        btn_popup1.setText(txtbtn1);
        btn_popup1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        btn_popup1.setWidth(screenwidth/2);

        btn_popup2 = new Button(this);
        btn_popup2.setText(txtbtn2);
        btn_popup2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        btn_popup2.setWidth(screenwidth/2);

        llayout.addView(btn_popup2);
        llayout.addView(btn_popup1);
    }


    private void retrieveData() {
        rootRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
              /*  mProgress = new ProgressDialog(BookTable.this);
                mProgress.setTitle("Updating tables");
                mProgress.show();
*/
                DataSnapshot order = dataSnapshot.child("ORDER");
                for(DataSnapshot single_order: order.getChildren())
                {
                    if(single_order.child("tableindex").getValue(Integer.class) == tableindex &&
                            single_order.child("spname").getValue().equals(currentspname)   )
                    {
                        for(DataSnapshot single_food: single_order.child("orderedFood").getChildren())
                        {
                            listData.add(single_food.getValue(Food.class));
                        }
                        numsguest = single_order.child("numguest").getValue(Integer.class);
                        Staff = single_order.child("staffname").getValue(String.class);

                    }

                }

                mProgress.dismiss();
                generateListOrdered();
                txtTotal.setText(Float.toString(totalprice));

                orderRef = rootRef.child("ORDER");
                txtStaff.setText(Staff);

                btnSend.setText("Thanh toán");


                OnBtnSendPay();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void OnBtnSendPay() {
        final String delOrderKey;
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteOldOrder();
                finish();

            }
        });
    }

    private void OnBtnSendClick() {
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderFormat myOrder = new OrderFormat();
                myOrder.setStaffname(Staff);
                myOrder.setOrderedFood(listData);
                myOrder.setTableindex(tableindex);
                myOrder.setNumguest(numsguest);

                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd>HH:mm:ss");
                String strDate = sdf.format(c.getTime());

                myOrder.setOrderedtime(strDate);
                myOrder.setSpname(currentspname);

                orderRef.push().setValue(myOrder);

                OrderBriefDescription setter = new OrderBriefDescription();
                setter.setSpname(currentspname);
                setter.setTableindex(tableindex);
                setter.setTotalpayment(totalprice);

                rootRef.child("details").push().setValue(setter);

                Intent i = new Intent(ReviewFoodActivity.this,BookTable.class);
                startActivity(i);
            }
        });
    }

    private void generateListOrdered() {
        totalprice = 0f;
        for(Food tmp:listData)
        {
            Float singleprice = Float.valueOf(Float.toString(tmp.getQuantity()*tmp.getPrize()));
            totalprice += singleprice;
            listOrdered.add(tmp.getFoodname()+" x "+tmp.getQuantity() + "= " + singleprice);

        }

    }

    @Override
    public void onBUttonClick(String type,int position, int lr,View v) {

        if(type.equals("separate")) {
            TextView left1 = (TextView) spdialog.findViewById(R.id.txt_left1);
            TextView left2 = (TextView) spdialog.findViewById(R.id.txt_left2);

            TextView right1 = (TextView) spdialog.findViewById(R.id.txt_right1);
            TextView right2 = (TextView) spdialog.findViewById(R.id.txt_right2);


            Button btnminus = (Button) v.findViewById(R.id.btn_minus);
            Button btnadd = (Button) v.findViewById(R.id.btn_add);


            if (lr == -1) //minus click
            {
                Food tmp = listSeparate.get(position);
                Food tmpchild = listchild.get(position);
                int quantity = tmp.getQuantity();
                int quantitychild = tmpchild.getQuantity();
                int max = listData.get(position).getQuantity();
                if (quantity <= max && quantitychild >0) {
                    quantity += 1;
                    tmp.setQuantity(quantity);
                    listSeparate.set(position, tmp);
                    separateadapter.notifyDataSetChanged();

                    quantitychild -= 1;
                    tmpchild.setQuantity(quantitychild);
                    listchild.set(position, tmpchild);


                    btnminus.setText(Integer.toString(quantity));
                    btnadd.setText(Integer.toString(quantitychild));

                    CalcMoneyndQuantity(left1, left2, listSeparate);
                    CalcMoneyndQuantity(right1, right2, listchild);
                }
            } else if (lr == 1) {
                Food tmp = listSeparate.get(position);
                Food tmpchild = listchild.get(position);
                int quantity = tmp.getQuantity();
                int quantitychild = tmpchild.getQuantity();
                int max = listData.get(position).getQuantity();
                if (quantity > 0 && quantitychild <= max) {
                    quantity -= 1;
                    tmp.setQuantity(quantity);
                    listSeparate.set(position, tmp);
                    separateadapter.notifyDataSetChanged();

                    quantitychild += 1;
                    tmpchild.setQuantity(quantitychild);
                    listchild.set(position, tmpchild);

                    btnminus.setText(Integer.toString(quantity));
                    btnadd.setText(Integer.toString(quantitychild));

                    CalcMoneyndQuantity(left1, left2, listSeparate);
                    CalcMoneyndQuantity(right1, right2, listchild);
                }

            }
            return;
        }
        else if (type.equals("checkorder")) {
            if (lr == -1) //minus click
            {
                Food tmp = listData.get(position);
                int quantity = tmp.getQuantity();

                if(quantity > 0) {
                    quantity -= 1;
                    tmp.setQuantity(quantity);
                    listData.set(position, tmp);
                }


            } else if (lr == 1) {
                Food tmp = listData.get(position);
                int quantity = tmp.getQuantity();
                quantity += 1;
                tmp.setQuantity(quantity);
                listData.set(position, tmp);

            }

            generateListOrdered();
            txtTotal.setText(Float.toString(totalprice));
        }

    }

    private void CalcMoneyndQuantity(TextView top, TextView bottom, ArrayList<Food> tmp) {
        int quantity = 0;
        float totalprice = 0f;
        for(Food a:tmp)
        {
            totalprice += a.getPrize()*a.getQuantity();
            quantity += a.getQuantity();
        }

        top.setText("Tổng tiền: " + Float.toString(totalprice));
        bottom.setText("Số lượng: " +Integer.toString(quantity));
    }
}
