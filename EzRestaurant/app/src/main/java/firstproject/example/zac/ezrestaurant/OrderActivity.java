package firstproject.example.zac.ezrestaurant;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class OrderActivity extends AppCompatActivity {

    private GridView gridCategories;
    private GridView gridFoodDetails;
    private Button btnSubmit;
    private  ArrayAdapter<String> adapterTypes;
    private  FoodItemAdapter adapterFood;
    private DatabaseReference rootRef;
    private ArrayList<String> listTypes = new ArrayList<>();
    private ArrayList<Food> listDetails = new ArrayList<>();
    private ArrayList<Food> listOrder = new ArrayList<>();
    private ArrayList<ArrayList<Food>> listFood = new ArrayList<ArrayList<Food>>();

    private int nums_guest;
    private int table_index;
    private String Staffname;
    private int typecall;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        //get data from intent BOOKTABLE
        Intent intent = getIntent();
        nums_guest =0;
        table_index = -1;
        if(intent != null)
        {
            nums_guest = Integer.parseInt( intent.getStringExtra("NUMS_GUEST"));
            table_index = intent.getIntExtra("TABLE_INDEX",-1);
            Staffname = intent.getStringExtra("Staff");
            typecall = intent.getIntExtra("TYPE",-1);

        }
        gridCategories = (GridView)findViewById(R.id.gridTypes);
        gridFoodDetails =  (GridView)findViewById(R.id.gridDetails);
        btnSubmit = (Button) findViewById(R.id.btn_submitorder);

        rootRef = FirebaseDatabase.getInstance().getReference();

        retrieveData();
        adapterTypes= new ArrayAdapter<String>(this,R.layout.single_foodtype,listTypes);

        adapterFood = new FoodItemAdapter(this,listDetails);
        gridCategories.setAdapter(adapterTypes);
        gridFoodDetails.setAdapter(adapterFood);

        OnGridTypeClick();
        OnGridFoodClick();
        OnSubmitBtnClick();


    }

    private void OnGridFoodClick() {
        gridFoodDetails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Food selectedFood = listDetails.get(position);
               Integer counter = selectedFood.getQuantity();
                listDetails.get(position).setQuantity(counter+1);
                adapterFood.notifyDataSetChanged();

               for(Food tmp_food:listOrder)
               {
                   if(tmp_food.getFoodname().equals(selectedFood.getFoodname())) {
                       tmp_food.setQuantity(tmp_food.getQuantity() );
                       return;
                   }
               }
                listOrder.add(listDetails.get(position));

            }
        });
    }

    private void OnSubmitBtnClick() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderFormat myOrder = new OrderFormat();
              /*  myOrder.setTable(1);*/
                myOrder.setOrderedFood(listOrder);


                Intent intent = new Intent(getBaseContext(),ReviewFoodActivity.class);
                intent.putParcelableArrayListExtra("ORDERLIST",myOrder.getOrderedFood());
                /*intent.putExtra("STAFF",Staffname);*/
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(OrderActivity.this);
                String staffname = sharedPref.getString(getString(R.string.saved_staff_name), "null");

                intent.putExtra("STAFF",staffname);
                intent.putExtra("NUMS_GUEST",nums_guest);
                intent.putExtra("TABLE_INDEX",table_index);
                if(typecall == 1002) //call from reviewFood
                {
                    ReviewFoodActivity a = new ReviewFoodActivity();
                    String spname =  "Table:" + table_index + " - (sp" + a.randomString(4) + ")";
                    intent.putExtra("SPNAME",spname);
                }
                else {
                    intent.putExtra("SPNAME","Table:" + table_index);
                }


                startActivity(intent);
            }
        });
    }

    private void OnGridTypeClick() {
        gridCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView txtFoodtype;
                for(int i =0;i< parent.getCount();i++) {
                    txtFoodtype= (TextView)parent.getChildAt(i).findViewById(R.id.txt_foodtype);
                    txtFoodtype.setBackgroundColor(Color.parseColor("#e5e5e5"));
                }
                txtFoodtype = (TextView) view.findViewById(R.id.txt_foodtype);
                txtFoodtype.setBackgroundColor(Color.parseColor("#ffcc33"));

                listDetails.clear();


                for(Food tmp:listFood.get(position))
                {
                    listDetails.add(tmp);
                }

                adapterFood.notifyDataSetChanged();

            }
        });
    }

    private void retrieveData() {
        rootRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                listTypes.clear();
              DataSnapshot food_type = dataSnapshot.child("Food");
                for(DataSnapshot tmp_type:food_type.getChildren())
                {
                    listTypes.add(tmp_type.getKey().toString());
                    ArrayList<Food> holder = new ArrayList<Food>();
                    for(DataSnapshot tmp_detail:tmp_type.getChildren())
                    {
                        Food f = new Food();
                        f.setFoodname(tmp_detail.getKey());
                        f.setPrize(tmp_detail.getValue(Float.class));
                        f.setQuantity(0);
                       holder.add(f);
                    }
                    listFood.add(holder);

                }
                adapterTypes.notifyDataSetChanged();
                adapterFood.notifyDataSetChanged();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {


            }
        });
    }


}
