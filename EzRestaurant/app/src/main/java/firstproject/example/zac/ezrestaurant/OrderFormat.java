package firstproject.example.zac.ezrestaurant;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Zac on 11/20/16.
 */

class Food implements Parcelable{
    private String Foodname;
    private int Quantity;
    private float Prize;
    private float discount;
    private String note;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
       Food f = new Food();
        f.setFoodname(this.getFoodname());
        f.setQuantity(this.getQuantity());
        f.setPrize(this.getPrize());
        f.setDiscount(this.getDiscount());
        f.setNote(this.getNote());
        f.setStatus(this.getStatus());
        return f;
    }

    public void setNote(String note) {
        this.note = note;
    }



    public String getNote() {
        return note;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getDiscount() {

        return discount;
    }

    public Food(){

        this.setStatus("");
        this.setNote("");
    }

    public void setPrize(float prize) {
        Prize = prize;
    }

    public float getPrize() {

        return Prize;
    }

    protected Food(Parcel in) {
        Foodname = in.readString();
        Quantity = in.readInt();
        Prize = in.readFloat();
        note = in.readString();
        status = in.readString();

    }

    public static final Creator<Food> CREATOR = new Creator<Food>() {
        @Override
        public Food createFromParcel(Parcel in) {
            return new Food(in);
        }

        @Override
        public Food[] newArray(int size) {
            return new Food[size];
        }
    };

    public void setFoodname(String foodname) {
        Foodname = foodname;
    }

    public void setQuantity(Integer quantity) {
        Quantity = quantity;
    }

    public String getFoodname() {

        return Foodname;
    }

    public Integer getQuantity() {
        return Quantity;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Foodname);
        dest.writeInt(Quantity);
        dest.writeFloat(Prize);
        dest.writeString(note);
        dest.writeString(status);
    }
}

class OrderBriefDescription implements Parcelable {

    protected OrderBriefDescription(Parcel in) {
        tableindex = in.readInt();
        totalpayment = in.readFloat();
        spname = in.readString();
    }

    public OrderBriefDescription()
    {

    }

    public static final Creator<OrderBriefDescription> CREATOR = new Creator<OrderBriefDescription>() {
        @Override
        public OrderBriefDescription createFromParcel(Parcel in) {
            return new OrderBriefDescription(in);
        }

        @Override
        public OrderBriefDescription[] newArray(int size) {
            return new OrderBriefDescription[size];
        }
    };

    public int getTableindex() {
        return tableindex;
    }

    public float getTotalpayment() {
        return totalpayment;
    }

    public String getSpname() {
        return spname;
    }

    private int tableindex;
    private float totalpayment;
    private String spname;

    public void setTableindex(int tableindex) {
        this.tableindex = tableindex;
    }

    public void setTotalpayment(float totalpayment) {
        this.totalpayment = totalpayment;
    }

    public void setSpname(String spname) {
        this.spname = spname;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(tableindex);
        dest.writeFloat(totalpayment);
        dest.writeString(spname);
    }
}
public class OrderFormat  {
    /*private int table;*/
    private ArrayList<Food> orderedFood;
    private String staffname;
    private String orderedtime;
    private int numguest;
    private int tableindex;

    private int total_quantity;

    public int getTotal_quantity() {
        return total_quantity;
    }

    public void setTotal_quantity(int total_quantity) {
        this.total_quantity = total_quantity;
    }

    private String spname; //separate table name

    public void setSpname(String spname) {
        this.spname = spname;
    }

    public String getSpname() {

        return spname;
    }

    public void setNumguest(int numguest) {
        this.numguest = numguest;
    }

    public void setTableindex(int tableindex) {
        this.tableindex = tableindex;
    }

    public int getNumguest() {

        return numguest;
    }

    public int getTableindex() {
        return tableindex;
    }

    public OrderFormat(){

    }

  /*  protected OrderFormat(Parcel in) {
        table = in.readInt();
        staffname = in.readString();
    }
*/


  /*  public void setTable(int table) {

        this.table = table;
    }*/

    public void setOrderedFood(ArrayList<Food> orderedFood) {
        this.orderedFood = orderedFood;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }

    public void setOrderedtime(String orderedtime) {
        this.orderedtime = orderedtime;
    }

    /*public int getTable() {

        return table;
    }*/

    public ArrayList<Food> getOrderedFood() {
        return orderedFood;
    }

    public String getStaffname() {
        return staffname;
    }

    public String getOrderedtime() {
        return orderedtime;
    }


}
